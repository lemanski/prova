package com.prova.app.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.view.RedirectView;

import com.prova.app.interfaces.IControllerModel;
import com.prova.app.model.Carrinho;

@Controller
public class CarrinhoController implements IControllerModel<Carrinho>{

	@Override
	@RequestMapping(value = {"/carrinho"}, method = RequestMethod.GET)
	public String page() {
		return "carrinho";
	}

	@Override
	public RedirectView save(Carrinho model) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Carrinho> delete(Carrinho model) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<List<Carrinho>> listAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Carrinho> findById(long modelId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Carrinho> change(Carrinho model) {
		// TODO Auto-generated method stub
		return null;
	}

}
