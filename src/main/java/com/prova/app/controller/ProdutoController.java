package com.prova.app.controller;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;

import com.prova.app.interfaces.IControllerModel;
import com.prova.app.model.Produto;
import com.prova.app.service.ProdutoService;

@Controller
public class ProdutoController implements IControllerModel<Produto>{

	@Autowired
	ProdutoService produtoService;
	
	@Override
	@RequestMapping(value = {"/cadastroProduto"}, method = RequestMethod.GET)
	public String page() {
		return "cadastroProduto";
	}

	@Override
	public RedirectView save(Produto model) {
		return null;
	}
	
	@RequestMapping(value = {"/produto/save"}, method = RequestMethod.POST)
	public RedirectView saveAll(Produto model, @RequestParam MultipartFile file) {
		produtoService.saveAll(model,file);
		return new RedirectView("/cadastroProduto");
	}

	@Override
	public ResponseEntity<Produto> delete(Produto model) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@RequestMapping(value = {"/produto/listAll"}, method = RequestMethod.GET)
	public ResponseEntity<List<Produto>> listAll() {
		List<Produto> produtos = produtoService.listAll();
		return new ResponseEntity<List<Produto>>(produtos,HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Produto> findById(long modelId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Produto> change(Produto model) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@RequestMapping(value = {"/produto/listAllCompra"}, method = RequestMethod.GET)
	public ResponseEntity<List<Produto>> findByProdutoCustoCompraNotNullOrProdutoCustoCompraGreaterThan() {
		List<Produto> produtos = produtoService.findByProdutoCustoCompraNotNullOrProdutoCustoCompraGreaterThan();
		return new ResponseEntity<List<Produto>>(produtos,HttpStatus.OK);
	}

	@RequestMapping(value = {"/produto/calculaPrecoUnit"}, method = RequestMethod.POST)
	public ResponseEntity<BigDecimal> calculaPrecoUnit(@RequestParam Long produtoId){
		BigDecimal precoUnit = produtoService.calculaPrecoUnit(produtoId);
		return new ResponseEntity<BigDecimal>(precoUnit, HttpStatus.OK);
	}
}
