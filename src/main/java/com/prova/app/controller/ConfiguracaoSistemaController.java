package com.prova.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.view.RedirectView;

import com.prova.app.interfaces.IControllerModel;
import com.prova.app.model.ConfiguracaoSistema;
import com.prova.app.service.ConfiguracaoSistemaService;

@Controller
public class ConfiguracaoSistemaController implements IControllerModel<ConfiguracaoSistema> {

	@Autowired
	private ConfiguracaoSistemaService configuracaoSistemaService;
	
	@Override
	@RequestMapping(value = {"/sistema"}, method = RequestMethod.GET)
	public String page() {
		return "configuracaoSistema";
	}

	@Override
	@RequestMapping(value = {"/configuracao/save"}, method = RequestMethod.POST)
	public RedirectView save(ConfiguracaoSistema model) {
		configuracaoSistemaService.save(model);
		return new RedirectView("/sistema");
	}

	@Override
	public ResponseEntity<ConfiguracaoSistema> delete(ConfiguracaoSistema model) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@RequestMapping(value = {"/configuracao/listAll"}, method = RequestMethod.GET)
	public ResponseEntity<List<ConfiguracaoSistema>> listAll() {
		List<ConfiguracaoSistema> configs = configuracaoSistemaService.listAll();
		return new ResponseEntity<List<ConfiguracaoSistema>>(configs,HttpStatus.OK);
	}

	@Override
	@RequestMapping(value = {"/configuracao/findById"}, method = RequestMethod.GET)
	public ResponseEntity<ConfiguracaoSistema> findById(long modelId) {
		return null;
	}

	@Override
	public ResponseEntity<ConfiguracaoSistema> change(ConfiguracaoSistema model) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@RequestMapping(value = {"/configuracao/findConfiguracao"}, method = RequestMethod.GET)
	public ResponseEntity<ConfiguracaoSistema> findConfiguracao() {
		ConfiguracaoSistema config = configuracaoSistemaService.findConfiguracao();
		return new ResponseEntity<ConfiguracaoSistema>(config,HttpStatus.OK);
	}

}
