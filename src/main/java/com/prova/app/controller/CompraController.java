package com.prova.app.controller;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.view.RedirectView;

import com.prova.app.interfaces.IControllerModel;
import com.prova.app.model.Compra;
import com.prova.app.service.CompraService;

@Controller
public class CompraController implements IControllerModel<Compra>{

	@Autowired
	private CompraService compraService;
	
	@Override
	@RequestMapping(value = {"/compras"}, method = RequestMethod.GET)
	public String page() {
		return "compras";
	}

	@Override
	@RequestMapping(value = {"/compras/save"}, method = RequestMethod.POST)
	public RedirectView save(Compra model) {
		compraService.save(model);
		return new RedirectView("/compras");
	}

	@Override
	public ResponseEntity<Compra> delete(Compra model) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@RequestMapping(value = {"/compras/listAll"}, method = RequestMethod.GET)
	public ResponseEntity<List<Compra>> listAll() {
		List<Compra> compras = compraService.listAll();
		return new ResponseEntity<List<Compra>>(compras,HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Compra> findById(long modelId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Compra> change(Compra model) {
		// TODO Auto-generated method stub
		return null;
	}

	@RequestMapping(value = {"/compras/calculaValorTotal"}, method = RequestMethod.GET)
	public ResponseEntity<BigDecimal> calculaValorTotal(){
		BigDecimal valorTotal = compraService.calculaValorTotal();
		valorTotal = valorTotal == null ? BigDecimal.ZERO : valorTotal;
		return new ResponseEntity<BigDecimal>(valorTotal,HttpStatus.OK);
	}
}
