package com.prova.app.interfaces;

import java.util.List;

public interface IServiceModel <G> {
	
	public G save(G model);
	public void delete(G model);
	public List<G> listAll();
	public G findById(Long modelId);
	public G change(G model);
}
