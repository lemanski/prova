package com.prova.app.interfaces;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.view.RedirectView;

public interface IControllerModel<G> {
	
//	@RequestMapping(value = {"/"}, method = RequestMethod.GET)
	public String page();
	
//		@RequestMapping(method=RequestMethod.POST,value= "/example",
//		consumes= MediaType.APPLICATION_JSON_VALUE)
	public RedirectView save(G model);

//		@RequestMapping(method=RequestMethod.DELETE,value= "/example",
//		consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<G> delete(G model);

//		@RequestMapping(method=RequestMethod.GET,value= "/example",
//		consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<G>> listAll();

//		@RequestMapping(method=RequestMethod.GET,value="/example/{id}",
//		consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<G> findById(@PathVariable long modelId);

//		@RequestMapping(method=RequestMethod.PUT,value= "/example",
//		consumes= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<G> change(G model);

//		Sample of return new ResponseEntity<Usuario>(usu,HttpStatus.OK)

}
