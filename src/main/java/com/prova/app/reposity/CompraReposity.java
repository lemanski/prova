package com.prova.app.reposity;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.prova.app.model.Compra;

public interface CompraReposity extends JpaRepository<Compra,Long>{

	@Query(value = "select sum(pedido_produto_quantidade * pedido_produto_preco_unitario) from tb_compra", nativeQuery = true)
	BigDecimal calculaValorTotal();

}
