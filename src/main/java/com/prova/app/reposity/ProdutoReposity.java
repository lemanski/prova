package com.prova.app.reposity;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.prova.app.model.Produto;

public interface ProdutoReposity extends JpaRepository<Produto,Long>{

	@Query(value = "select * from tb_produto where produto_custo_compra is not null and produto_custo_compra > 0 order by produto_nome asc", nativeQuery = true)
	List<Produto> findByProdutoCustoCompraNotNullOrProdutoCustoCompraGreaterThan();
	
	@Query(value = "select count(*) from tb_produto where produto_custo_compra is not null and produto_custo_compra > 0", nativeQuery = true)
	Integer countAllProdutosCustoCompraGreaterThanZero();

}
