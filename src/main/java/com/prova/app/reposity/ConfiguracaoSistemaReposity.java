package com.prova.app.reposity;

import org.springframework.data.jpa.repository.JpaRepository;

import com.prova.app.model.ConfiguracaoSistema;

public interface ConfiguracaoSistemaReposity extends JpaRepository<ConfiguracaoSistema,Long>{

}
