package com.prova.app.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tb_compra")
public class Compra {
	
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Long id;
	
	@Column(name="pedido_produto_quantidade")
	private Long pedidoProdutoQuantidade;
	
	@OneToOne
	@JoinColumn(name="produto")
	private Produto produto;
	
	@Column(name="pedido_produto_preco_unitario")
	private BigDecimal pedidoProdutoPrecoUnitario;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPedidoProdutoQuantidade() {
		return pedidoProdutoQuantidade;
	}

	public void setPedidoProdutoQuantidade(Long pedidoProdutoQuantidade) {
		this.pedidoProdutoQuantidade = pedidoProdutoQuantidade;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public BigDecimal getPedidoProdutoPrecoUnitario() {
		return pedidoProdutoPrecoUnitario;
	}

	public void setPedidoProdutoPrecoUnitario(BigDecimal pedidoProdutoPrecoUnitario) {
		this.pedidoProdutoPrecoUnitario = pedidoProdutoPrecoUnitario;
	}

}
