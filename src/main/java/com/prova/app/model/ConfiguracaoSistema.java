package com.prova.app.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_configuracao_sistema")
public class ConfiguracaoSistema {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private Long id;
	
	@Column(name="configuracao_despesas_totais")
	private BigDecimal configuracaoDespesasTotais;
	
	@Column(name="configuracao_margem_lucro")
	private BigDecimal configuracaoMargemLucro;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getConfiguracaoDespesasTotais() {
		return configuracaoDespesasTotais;
	}

	public void setConfiguracaoDespesasTotais(BigDecimal configuracaoDespesasTotais) {
		this.configuracaoDespesasTotais = configuracaoDespesasTotais;
	}

	public BigDecimal getConfiguracaoMargemLucro() {
		return configuracaoMargemLucro;
	}

	public void setConfiguracaoMargemLucro(BigDecimal configuracaoMargemLucro) {
		this.configuracaoMargemLucro = configuracaoMargemLucro;
	}
}
