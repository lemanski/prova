package com.prova.app.model;

import java.math.BigDecimal;
import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name="tb_produto")
public class Produto {
	
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Long id;
	
	@Column(name="produto_nome")
	private String produtoNome;
	
	@Column(name="produto_custo_compra")
	private BigDecimal produtoCustoCompra;
	
	@Column(name="produto_descricao")
	private String produtoDescricao;
	
	@Lob 
	@Column(name="produto_imagem") 
	private byte[] produtoImagem;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProdutoNome() {
		return produtoNome;
	}

	public void setProdutoNome(String produtoNome) {
		this.produtoNome = produtoNome;
	}

	public BigDecimal getProdutoCustoCompra() {
		return produtoCustoCompra;
	}

	public void setProdutoCustoCompra(BigDecimal produtoCustoCompra) {
		this.produtoCustoCompra = produtoCustoCompra;
	}

	public String getProdutoDescricao() {
		return produtoDescricao;
	}

	public void setProdutoDescricao(String produtoDescricao) {
		this.produtoDescricao = produtoDescricao;
	}

	public byte[] getProdutoImagem() {
		return produtoImagem;
	}

	public void setProdutoImagem(byte[] produtoImagem) {
		this.produtoImagem = produtoImagem;
	}

}