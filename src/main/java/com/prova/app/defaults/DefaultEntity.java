package com.prova.app.defaults;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class DefaultEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "ts_cad")
	private LocalDateTime dataCadastro;

	@Column(name = "ts_atu")
	private LocalDateTime dataAtualizacao;

	@Column(name = "isActive")
	private char isActive;

	@Column(name = "isExcluido")
	private char isExcluido;

	public LocalDateTime getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDateTime dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public LocalDateTime getDataAtualizacao() {
		return dataAtualizacao;
	}

	public void setDataAtualizacao(LocalDateTime dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	public char getIsExcluido() {
		return isExcluido;
	}

	public void setIsExcluido(char isExcluido) {
		this.isExcluido = isExcluido;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public char getIsActive() {
		return isActive;
	}

	public void setIsActive(char isActive) {
		this.isActive = isActive;
	}
}
