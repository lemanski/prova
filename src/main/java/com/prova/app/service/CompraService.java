package com.prova.app.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prova.app.interfaces.IServiceModel;
import com.prova.app.model.Compra;
import com.prova.app.reposity.CompraReposity;

@Service
public class CompraService implements IServiceModel<Compra>{

	@Autowired
	CompraReposity compraReposity;
	
	@Override
	public Compra save(Compra model) {
		if(model != null && model.getPedidoProdutoQuantidade() != null && model.getPedidoProdutoQuantidade() == 0 ) {
			return null;
		}
		return compraReposity.save(model);
	}

	@Override
	public void delete(Compra model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Compra> listAll() {
		return compraReposity.findAll();
	}

	@Override
	public Compra findById(Long modelId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Compra change(Compra model) {
		// TODO Auto-generated method stub
		return null;
	}

	public BigDecimal calculaValorTotal() {
		return compraReposity.calculaValorTotal();
	}

}
