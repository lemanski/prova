package com.prova.app.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prova.app.interfaces.IServiceModel;
import com.prova.app.model.ConfiguracaoSistema;
import com.prova.app.reposity.ConfiguracaoSistemaReposity;

@Service
public class ConfiguracaoSistemaService  implements IServiceModel<ConfiguracaoSistema>{

	@Autowired
	private ConfiguracaoSistemaReposity configuracaoSistemaReposity;
	
	@Override
	public ConfiguracaoSistema save(ConfiguracaoSistema model) {
		List<ConfiguracaoSistema> config = configuracaoSistemaReposity.findAll();
		if(model.getConfiguracaoDespesasTotais().equals(BigDecimal.ZERO)) {
			model.setConfiguracaoDespesasTotais(new BigDecimal("400"));
		}
		if(config.isEmpty()) {
			return configuracaoSistemaReposity.save(model);
		}else {
			for(ConfiguracaoSistema conf : config) {
				conf.setConfiguracaoDespesasTotais(model.getConfiguracaoDespesasTotais());
				conf.setConfiguracaoMargemLucro(model.getConfiguracaoMargemLucro());
				return configuracaoSistemaReposity.save(conf);
			}
			return null;
		}
	}

	@Override
	public void delete(ConfiguracaoSistema model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<ConfiguracaoSistema> listAll() {
		return configuracaoSistemaReposity.findAll();
	}

	@Override
	public ConfiguracaoSistema findById(Long modelId) {
		return null;
	}
	

	@Override
	public ConfiguracaoSistema change(ConfiguracaoSistema model) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public ConfiguracaoSistema findConfiguracao() {
		List<ConfiguracaoSistema> config = listAll();
		ConfiguracaoSistema configuracao = new ConfiguracaoSistema();
		for(ConfiguracaoSistema conf : config) {
			configuracao.setConfiguracaoDespesasTotais(conf.getConfiguracaoDespesasTotais());
			configuracao.setConfiguracaoMargemLucro(conf.getConfiguracaoMargemLucro());
			configuracao.setId(conf.getId());
		}
		return configuracao;
	}

}
