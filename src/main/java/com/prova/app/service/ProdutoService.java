package com.prova.app.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Blob;
import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.prova.app.interfaces.IServiceModel;
import com.prova.app.model.ConfiguracaoSistema;
import com.prova.app.model.Produto;
import com.prova.app.reposity.ProdutoReposity;

@Service
public class ProdutoService implements IServiceModel<Produto>{

	@Autowired
	ProdutoReposity produtoReposity;
	
	@Autowired
	ConfiguracaoSistemaService configuracaoSistemaService;
	
	@Autowired
	private ServletContext context;
	
	@Override
	public Produto save(Produto model) {
		return produtoReposity.save(model);
	}
	@Override
	public void delete(Produto model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Produto> listAll() {
		return produtoReposity.findAll();
	}

	@Override
	public Produto findById(Long modelId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Produto change(Produto model) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Produto> findByProdutoCustoCompraNotNullOrProdutoCustoCompraGreaterThan() {
		return produtoReposity.findByProdutoCustoCompraNotNullOrProdutoCustoCompraGreaterThan();
	}

	public BigDecimal calculaPrecoUnit(Long produtoId) {
		Produto prod = produtoReposity.getOne(produtoId);
		ConfiguracaoSistema conf = configuracaoSistemaService.findConfiguracao();
		Integer quantidadeProduto = produtoReposity.countAllProdutosCustoCompraGreaterThanZero();
		BigDecimal rateio = BigDecimal.ZERO;
		if(conf != null && conf.getConfiguracaoDespesasTotais() != null) {
			rateio = conf.getConfiguracaoDespesasTotais().divide(new BigDecimal(quantidadeProduto.toString()), 2, RoundingMode.HALF_UP);
			rateio = rateio.add(prod.getProdutoCustoCompra());
		}
		if(conf != null && conf.getConfiguracaoMargemLucro() != null && conf.getConfiguracaoMargemLucro() != BigDecimal.ZERO) {
			return rateio.multiply(new BigDecimal("1").add(conf.getConfiguracaoMargemLucro()));
		}else {
			return rateio;
		}
	}
	public void saveAll(Produto model, MultipartFile file) {
		try {
			byte[] imagem = file.getBytes();
			model.setProdutoImagem(imagem);
		} catch (Exception e) {
			e.printStackTrace();
		}
		produtoReposity.save(model);
	}
	
}
