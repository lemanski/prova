let table;
$( document ).ready(function() {
	configuracaoInicialMenu();
	gridCadastro();
});

function configuracaoInicialMenu(){
	$("[id='dropDown']").show();
	$("[id='cadastros']")[0].style.color = 'white';
	$("[id='cadastroProduto']")[0].style.color = 'white';
}

function gridCadastro(){
	$('#grid_cadastro').DataTable( {
    	"processing": false,
        "serverSide": false,
        "ajax": {url: '/produto/listAll',dataSrc: "",mDataProp: "",type: 'GET'},
        rowId: 'id',
        select: true,
        dom: 'Bfrtip',
        aoColumns: [
        	{ mData: "produtoNome" },
            { mData: "produtoCustoCompra"},
            { mData: "produtoDescricao" }
        ],
        "oLanguage": {
 		   "sUrl": "/javascript/plugins/traducao.json"
     	 }
//        ,
//		 "columnDefs": [
//			    {"render": function ( data, type, row ) {
//			    	if(row.produtoImagem){
//			    		return '<img src="data:image/bmp;base64,'+row.produtoImagem+'" width="10" height="10" />';},"targets": 3}			    		
//			    	}else{
//			    		return 'Imagem Não disponivel!';
//			    	}
//			  ]
    } );
}
function fileChange(){
	$('#fileName').text($("#file").val().split('\\')[2]);
}



