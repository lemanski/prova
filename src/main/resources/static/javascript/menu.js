let controleDropDown = false;
function mostrarDropDown(){
	if(controleDropDown) {
		$("[id='dropDown']").hide();
		controleDropDown = false;
	}else{
		$("[id='dropDown']").show();
		controleDropDown = true;
	}
}
$( document ).ready(function() {
    var form = document.querySelector("form");
    form.onsubmit = submitted.bind(form);
});

function submitted(event) {
	$.blockUI({ message: '<h1>Carregando... Aguarde!</h1>' });
}

function onlyLetters(evt)
{
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode != 46 && charCode > 31 
	&& (charCode < 48 || charCode > 57))
	return true;
	return false;
}

function onlyNumber(evt)
{
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode != 46 && charCode > 31 
	&& (charCode < 48 || charCode > 57))
	return false;
	return true;
} 