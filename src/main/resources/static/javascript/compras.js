let table;
$( document ).ready(function() {
	configuracaoInicialMenu();
	comboProduto();
	$("[id='produto']").change(function(e){
		calculaPreco();
	});
	gridCadastro();
	calcularValorTotal();
});

function configuracaoInicialMenu(){
	$("[id='nav-compras']")[0].style.color = 'white';
}
function comboProduto() {
	let html_code = '';
	return $.getJSON("/produto/listAllCompra", function(data){
		$.each(data, function(key, value){
			if(value !== undefined){
				html_code += '<option value="'+value.id+'">'+value.produtoNome.toUpperCase();+'</option>';
				$("[id='produto']").html(html_code);											
			}
		});
		calculaPreco();
	});
}
function calculaPreco(){
	calculaPrecoUnit().then(function(){
		calculaPrecoProdutoTotal();
	});
}
function calculaPrecoUnit(){
	let produtoId = $('#produto').val();
	return $.post("/produto/calculaPrecoUnit",{ produtoId : produtoId}, function(data){
		$('#pedidoProdutoPrecoUnitario').val(data);
	});
}
function calculaPrecoProdutoTotal(){
	let valorUnit = $('#pedidoProdutoPrecoUnitario').val();
	let qtd = $('#pedidoProdutoQuantidade').val();
	let valorTotal = valorUnit * qtd;
	$('#pedidoProdutoPrecoTotal').val(valorTotal);
}
function gridCadastro(){
	$('#grid_cadastro').DataTable( {
    	"processing": false,
        "serverSide": false,
        "ajax": {url: '/compras/listAll',dataSrc: "",mDataProp: "",type: 'GET'},
        rowId: 'id',
        select: true,
        dom: 'Bfrtip',
        aoColumns: [
        	{ mData: "pedidoProdutoQuantidade"},
        	{ mData: "produto.produtoNome" },
            { mData: "pedidoProdutoPrecoUnitario" }
        ],
        "oLanguage": {
 		   "sUrl": "/javascript/plugins/traducao.json"
     	 },
     	"columnDefs": [
		    {"render": function ( data, type, row ) {
		    	debugger; return (row.pedidoProdutoPrecoUnitario * row.pedidoProdutoQuantidade);},"targets": 3}
		  ]
    } );
}
function calcularValorTotal() {
	return $.getJSON("/compras/calculaValorTotal",function(data){
		$('#pedidoProdutoValorToral').val(data);
	});
}