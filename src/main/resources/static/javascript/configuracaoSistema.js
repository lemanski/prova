let table;
$( document ).ready(function() {
	configuracaoInicialMenu();
	loadPageConfig();
});

function configuracaoInicialMenu(){
	$("[id='nav-configuracao']")[0].style.color = 'white';
}

function loadPageConfig() {
	return $.getJSON("/configuracao/findConfiguracao", function(data){
		$.each(data, function(key, value){
			if(value !== undefined && key === 'configuracaoDespesasTotais'){
				$('#configuracaoDespesasTotais').val(value);
			}else
				if(value !== undefined && key === 'configuracaoMargemLucro'){
					$('#configuracaoMargemLucro').val(value);
				}
		});
	});
}


