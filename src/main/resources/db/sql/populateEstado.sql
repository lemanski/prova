

INSERT INTO tb_estado (id,estado_uf,estado_codigo_ibge,estado_nome,pais) VALUES
(nextval('hibernate_sequence'),'RO',11,'Rondônia',30)
,(nextval('hibernate_sequence'),'AC',12,'Acre',30)
,(nextval('hibernate_sequence'),'AM',13,'Amazonas',30)
,(nextval('hibernate_sequence'),'RR',14,'Roraima',30)
,(nextval('hibernate_sequence'),'PA',15,'Pará',30)
,(nextval('hibernate_sequence'),'AP',16,'Amapá',30)
,(nextval('hibernate_sequence'),'TO',17,'Tocantins',30)
,(nextval('hibernate_sequence'),'MA',21,'Maranhão',30)
,(nextval('hibernate_sequence'),'PI',22,'Piauí',30)
,(nextval('hibernate_sequence'),'CE',23,'Ceará',30)
;
INSERT INTO tb_estado (id,estado_uf,estado_codigo_ibge,estado_nome,pais) VALUES
(nextval('hibernate_sequence'),'RN',24,'Rio Grande do Norte',30)
,(nextval('hibernate_sequence'),'PB',25,'Paraíba',30)
,(nextval('hibernate_sequence'),'PE',26,'Pernambuco',30)
,(nextval('hibernate_sequence'),'AL',27,'Alagoas',30)
,(nextval('hibernate_sequence'),'SE',28,'Sergipe',30)
,(nextval('hibernate_sequence'),'BA',29,'Bahia',30)
,(nextval('hibernate_sequence'),'MG',31,'Minas Gerais',30)
,(nextval('hibernate_sequence'),'ES',32,'Espirito Santo',30)
,(nextval('hibernate_sequence'),'RJ',33,'Rio de Janeiro',30)
,(nextval('hibernate_sequence'),'SP',35,'São Paulo',30)
;
INSERT INTO tb_estado (id,estado_uf,estado_codigo_ibge,estado_nome,pais) VALUES
(nextval('hibernate_sequence'),'PR',41,'Paraná',30)
,(nextval('hibernate_sequence'),'SC',42,'Santa Catarina',30)
,(nextval('hibernate_sequence'),'RS',43,'Rio Grande do Sul',30)
,(nextval('hibernate_sequence'),'MS',50,'Mato Grosso do Sul',30)
,(nextval('hibernate_sequence'),'MT',51,'Mato Grosso',30)
,(nextval('hibernate_sequence'),'GO',52,'Goiás',30)
,(nextval('hibernate_sequence'),'DF',53,'Distrito Federal',30)
;