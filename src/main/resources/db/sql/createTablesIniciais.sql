CREATE SEQUENCE hibernate_sequence START WITH 1 INCREMENT BY 1;

CREATE TABLE IF NOT EXISTS tb_pais (
	id int8 NOT NULL,
	ts_atu timestamp NULL,
	ts_cad timestamp NULL,
	is_active bpchar(1) default 't' NULL,
	is_excluido bpchar(1) default 'f' NULL,
	pais_idioma varchar(255) NULL,
	pais_nome varchar(255) NULL,
	pais_sigla varchar(255) NULL,
	CONSTRAINT tb_pais_pkey PRIMARY KEY (id)
);

ALTER TABLE tb_pais OWNER TO postgres;
GRANT ALL ON TABLE tb_pais TO postgres;

CREATE TABLE IF NOT EXISTS tb_estado (
	id int8 NOT NULL,
	ts_atu timestamp NULL,
	ts_cad timestamp NULL,
	is_active bpchar(1) default 't' NULL,
	is_excluido bpchar(1) default 'f' NULL,
	estado_codigo_ibge varchar(255) NULL,
	estado_nome varchar(255) NULL,
	estado_uf varchar(255) NULL,
	pais int8 NULL,
	CONSTRAINT tb_estado_pkey PRIMARY KEY (id)
);

ALTER TABLE tb_estado OWNER TO postgres;
GRANT ALL ON TABLE tb_estado TO postgres;

CREATE TABLE IF NOT EXISTS tb_cidade (
	id int8 NOT NULL,
	ts_atu timestamp NULL,
	ts_cad timestamp NULL,
	is_active bpchar(1) default 't' NULL,
	is_excluido bpchar(1) default 'f' NULL,
	cidade_codigo_ibge varchar(255) NULL,
	cidade_nome varchar(255) NULL,
	estado int8 NULL,
	CONSTRAINT tb_cidade_pkey PRIMARY KEY (id)
);

ALTER TABLE tb_cidade OWNER TO postgres;
GRANT ALL ON TABLE tb_cidade TO postgres;


CREATE TABLE IF NOT EXISTS tb_cliente_contato (
	id int8 NOT NULL,
	ts_atu timestamp NULL,
	ts_cad timestamp NULL,
	is_active bpchar(1) NULL,
	is_excluido bpchar(1) NULL,
	cliente int8 NULL,
	contato_email varchar(255) NULL,
	contato_site varchar(255) NULL,
	contato_telefone varchar(255) NULL,
	CONSTRAINT tb_cliente_contato_pkey PRIMARY KEY (id)
);

ALTER TABLE tb_cliente_contato OWNER TO postgres;
GRANT ALL ON TABLE tb_cliente_contato TO postgres;

CREATE TABLE IF NOT EXISTS tb_cliente_endereco (
	id int8 NOT NULL,
	ts_atu timestamp NULL,
	ts_cad timestamp NULL,
	is_active bpchar(1) NULL,
	is_excluido bpchar(1) NULL,
	cidade int8 NULL,
	cliente int8 NULL,
	endereco_bairro varchar(255) NULL,
	endereco_cep varchar(255) NULL,
	endereco_logradouro varchar(255) NULL,
	endereco_numero varchar(255) NULL,
	CONSTRAINT tb_cliente_endereco_pkey PRIMARY KEY (id)
);

ALTER TABLE tb_cliente_endereco OWNER TO postgres;
GRANT ALL ON TABLE tb_cliente_endereco TO postgres;


CREATE TABLE IF NOT EXISTS tb_cliente (
	id int8 NOT NULL,
	ts_atu timestamp NULL,
	ts_cad timestamp NULL,
	is_active bpchar(1) NULL,
	is_excluido bpchar(1) NULL,
	cliente_contato int8 NULL,
	cliente_documento_numero varchar(255) NULL,
	cliente_endereco int8 NULL,
	cliente_is_pessoa_fisica bpchar(1) NULL,
	cliente_nome varchar(255) NULL,
	CONSTRAINT tb_cliente_pkey PRIMARY KEY (id)
);

ALTER TABLE tb_cliente OWNER TO postgres;
GRANT ALL ON TABLE tb_cliente TO postgres;